
Ti.include('/view/marretaview.js');

var pancada1 = Titanium.Media.createSound({
	url: 'pancada1.mp3',
});

var pancada2 = Titanium.Media.createSound({
	url: 'pancada2.mp3',
});
var pancada3 = Titanium.Media.createSound({
	url: 'pancada3.mp3',
});
var ventogolpe1 = Titanium.Media.createSound({
	url: 'ventogolpe1.mp3',
});

var ventogolpe2 = Titanium.Media.createSound({
	url: 'ventogolpe2.mp3',
});
var anteninhasplay = Titanium.Media.createSound({
	url: 'sons/chapolin_anteninhas.mp3',
});
var astuciaplay = Titanium.Media.createSound({
	url: 'sons/chapolin_astucia.mp3',
});
var eusouplay = Titanium.Media.createSound({
	url: 'sons/chapolin_eu_sou_chapolin.mp3',
});

var movimentosplay = Titanium.Media.createSound({
	url: 'sons/chapolin_movimentos_calculados.mp3',
});

var palmaplay = Titanium.Media.createSound({
	url: 'sons/chapolin_palma.mp3',
});

var euplay = Titanium.Media.createSound({
	url: 'sons/chapolin_eu.mp3',
});
var cores = ["yellow", "white", "grey"];

var pass=true;
var pass2=true;

var pass3=true;
var pass4=true;
var pass5=true;

var accelerometerCallback = function(e) {
  // labelTimestamp.text = 'timestamp: ' + e.timestamp;
  // labelx.text = 'x: ' + e.x;
  // labely.text = 'y: ' + e.y;
  // labelz.text = 'z: ' + e.z;
  // Titanium.API.log("x = ",e.x, " y = ",e.y," z = ",e.z);

// ========   golpe frontal
  if(e.x>=19&&e.z<10&&e.z>-17&&pass==true){
  	pass=false;
  	pancada1.stop();
  	pancada1.play();

    Ti.Media.vibrate([0,1000]);

efeitosvisuaisvibracoes();


  }else if(e.x<=-4)
  {
  	pass=true;
  }

	// if(e.x>=3&&e.z<5&&e.z>-10&&pass5==true){
	// 	pass5=false;
	// 	ventogolpe1.stop();
	// 	ventogolpe1.play();
	//
	//
	// }else if(e.z<=-4)
	// {
	// 	pass5=true;
	// }


	if(e.z>=10&&pass3==true){
		pass3=false;
		ventogolpe1.stop();
		ventogolpe1.play();


	}else if(e.z<=-10)
	{
		pass3=true;
	}


	if(e.z<=-10&&pass4==true){
		pass4=false;
		ventogolpe2.stop();
		ventogolpe2.play();


	}else if(e.z>=10)
	{
		pass4=true;
	}

////====================== GOLPE LATERAL
  if(e.x>=19&&e.z>=19&&pass2==true){
  	pass2=false;
  	pancada2.stop();
  	pancada2.play();
    Ti.Media.vibrate([0,1000]);
efeitosvisuaisvibracoes();
  }else if(e.x<=-4)
  {
  	pass2=true;
  }

	if(e.x>=19&&e.z<=-17&&pass2==true){
  	pass2=false;
  	pancada3.stop();
  	pancada3.play();
    Ti.Media.vibrate([0,1000]);
efeitosvisuaisvibracoes();
  }else if(e.x<=-4)
  {
  	pass2=true;
  }
};


function efeitosvisuaisvibracoes(){
	var animation = Titanium.UI.createAnimation();
		animation.backgroundColor = cores[getRandomInt(0,2)];
		animation.duration = 150;
	var animationHandler = function() {
		  animation.removeEventListener('complete',animationHandler);
		  animation.backgroundColor = 'black';
		   images.animate(animation);
		};
		animation.addEventListener('complete',animationHandler);
		images.animate(animation);

		var animation2 = Titanium.UI.createAnimation();
			animation2.opacity = 1;
			animation2.duration = 300;
		var animationHandler2 = function() {
			  animation2.removeEventListener('complete',animationHandler2);
			  animation2.opacity = 0;
			   view.animate(animation2);
			};
			animation2.addEventListener('complete',animationHandler2);
			view.animate(animation2);

}


if (Ti.Platform.model === 'Simulator' || Ti.Platform.model.indexOf('sdk') !== -1 ){
  alert('Accelerometer does not work on a virtual device');
} else {
  Ti.Accelerometer.addEventListener('update', accelerometerCallback);
  if (Ti.Platform.name === 'android'){
    Ti.Android.currentActivity.addEventListener('pause', function(e) {
      Ti.API.info("removing accelerometer callback on pause");
      Ti.Accelerometer.addEventListener('update', accelerometerCallback);
    });
    Ti.Android.currentActivity.addEventListener('resume', function(e) {
      Ti.API.info("adding accelerometer callback on resume");
      Ti.Accelerometer.addEventListener('update', accelerometerCallback);
    });
  }
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
